function [ws_loc, wd_loc, ti_loc, proj_distance] = florisInSimulinkWrapper(ws_fs,wd_fs,ti_fs,cT,nac_orient)
% The output of this function should be column vectors only


pyOut = py.run_floris.f(ws_fs,wd_fs,ti_fs,cT,nac_orient);
pyOut = cell(pyOut);

ws_loc = pyOut{1,1}; ws_loc = cellfun(@double,cell(ws_loc));
wd_loc = pyOut{1,2}; wd_loc = cellfun(@double,cell(wd_loc));
ti_loc = pyOut{1,3}; ti_loc = cellfun(@double,cell(ti_loc));
proj_distance = pyOut{1,4}; proj_distance = cellfun(@double,cell(proj_distance));

ws_loc = ws_loc';
wd_loc = wd_loc';
ti_loc = ti_loc';
proj_distance = [0,proj_distance]';
end