# -*- coding: utf-8 -*-
"""
Created on Mon Jan 17 14:52:45 2022

@author: Abhinav Anand, Robert Braunbehrens
         Chair of Wind Energy,
         Technical University of Munich
"""


import floris.tools as wfct
import numpy as np
import sys
from rotor_quantities import get_vel_at_rotor


def f(ws, wd, ti, ct_list, nac_orient):
    fi = wfct.floris_interface.FlorisInterface("example_input.json")
    
    
    #### Attempt 2 ####
    
    #----------------Calculate projected distance between the turbines---------------------------------------- 
    layout_x = fi.layout_x 
    layout_y = fi.layout_y
    ortho_distance = np.diff(layout_x) # This is true only for the case when all the turbines are in a single row
    actual_distance = np.zeros(len(layout_x)-1)
    for i in range(0,len(layout_x)-1):
        actual_distance[i] = np.sqrt(np.power((layout_x[i+1]-layout_x[i]),2) + np.power((layout_y[i+1]-layout_y[i]),2))
        
    angle_horizontal = np.arccos(ortho_distance/actual_distance) # In radians
    angle_inflow = (wd-270) # in degrees
    angle_inflow = angle_inflow*(np.pi/180) # converting degrees to radians
    resulting_angle = angle_horizontal-angle_inflow
    
    proj_distance = actual_distance*np.cos(resulting_angle)
    proj_distance = proj_distance.tolist()
    actual_distance = actual_distance.tolist()
    
    
    #----------------Reinitialize flow field---------------------------------------- 
    fi.reinitialize_flow_field( 
                               wind_direction = wd,
                               wind_speed = ws,
                               turbulence_intensity= ti
                               )
    
    #----------------Change the CT curve---------------------------------------- 
    power_thrust_table = fi.floris.farm.turbines[0].power_thrust_table
    for i, ct in enumerate(ct_list):
        power_thrust_table["thrust"] = [ct for j in power_thrust_table["thrust"]]  #sets all elements of the curve to "ct"
        turbine_change_dict = {"power_thrust_table" : power_thrust_table}
        fi.change_turbine([i], turbine_change_dict) 

    #----------------Calculate Wake---------------------------------------- 
    yaw = [wd-i for i in nac_orient] # yaw misalingnment
    # print(yaw)
    fi.calculate_wake(yaw_angles = yaw) 
    # fi.show_flow_field() # This causes MATLAB to crash if we are calling Python from MATLAB
    
    
    
    #----------------Get Local Velocity---------------------------------------- 
    ws_loc = [i.average_velocity for i in fi.floris.farm.turbines]
    
    #-------------Get local turbulence intensity-------------------------------
    ti_loc = [i.current_turbulence_intensity for i in fi.floris.farm.turbines] # This is wake-influenced TI
    # ti_loc = fi.floris.farm.turbulence_intensity # This is ambient TI




    # #-------------Get local wind direction-------------------------------------
    wd_loc = []
    for i in range(len(layout_x)):
        tab = get_vel_at_rotor(fi, i, visualization = False)
        u_mean = np.mean(tab["u"])    
        v_mean = np.mean(tab["v"]) #looking downwind when v points to the left it should be positive
        wd_loc_i =  np.round(np.mod(wd - np.arctan(v_mean/u_mean) * 180/np.pi, 360),2)
        wd_loc.append(wd_loc_i)
    
            
    return ws_loc, wd_loc, ti_loc, proj_distance


    
def f1():
    fi = wfct.floris_interface.FlorisInterface("example_input.json")
    #fi.reinitialize_flow_field()
    fi.calculate_wake() # Calculate wake
    hor_plane = fi.get_hor_plane() # Get horizontal plane at default height (hub-height)
    #print(hor_plane.df.values)
    return hor_plane.df


if __name__ == '__main__':
    wind_speed = 9
    wind_direction = 270
    turbulence_intensity = 0.07
    thrust_coeffs = [0.5,0.5,0.5,0.5]
    nac_orient = [240,270,270,270]
    yaw_angles = [wind_direction-i for i in nac_orient]

    sys.stdout.write(str(f(wind_speed, wind_direction, turbulence_intensity, thrust_coeffs, nac_orient)))  
    #sys.stdout.write(str(f1()))
    #fi.show_flow_field()
    
    