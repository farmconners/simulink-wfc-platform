# Open access Simulink WFC platform

FarmConners open access Simulink model towards **Integrated Wind Farm control**

DTU Wind Energy
Thanasis Barlas, Qian Long, Kaushik Das, Tuhfe Göcmen
TUM Chair of Wind energy
Abhinav Anand, Filippo Campagnolo

Basic summary of contents:
- wind power plant controller with MPPT, balance and delta control modes
- DTU WE wind turbine controller (with derating and yaw control features)
- simple 1-DOF drivetrain WT model with Cp/Ct=f(TSR, pitch, yaw) tabulated input
- FLORIS wind farm flow model integration
- IEA 15MW RWT modelled as an example

Quick user guide:
- read 'HowTo_FlorisInSimulink.pdf' for FLORIS Python integration
- these data files need to be present : WT_data.mat, wpdata.100, drdata.dat, InflowData.mat, example_input.json (see specified variables and format in the example files)
- set input in 'WPP_integrated_input.m'
- open 'WPP_integrated_with_FLORIS.mdl' and run
- basic variables are plotted in scopes and exported to the workspace

![flyer](Additional_files/UmbrellaFigure.jpg)
*Eguinoa, I, Göçmen, T, Garcia-Rosa, PB, et al. Wind farm flow control oriented to electricity markets and grid integration: Initial perspective analysis. Adv Control Appl. 2021 (https://doi.org/10.1002/adc2.80)*

<img align="left" src="Additional_files/flag_yellow_high.jpg"  width="100">

The FarmConners project has received funding from the European Union's Horizon 2020 research and innovation programme under grant agreement No. 857844.
