#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 17 14:52:45 2022

@author: Abhinav Anand, Robert Braunbehrens
         Chair of Wind Energy
         Technical University of Munich
"""
import numpy as np
import matplotlib.pyplot as plt

def get_vel_at_rotor(fi,i_turb, visualization = False):
    """
    This function should return the velocities directly in front of the rotor throught the fi.get_set_of_points function
    """


    #3D rotor discretization
    Np_y = 10
    Np_z = 11
    
    #Floris information
    t = fi.floris.farm.turbines[i_turb]
    rotor_diameter = t.rotor_diameter
    zHub = t.hub_height
    wind_direction = np.mod(270+fi.floris.farm.wind_direction[i_turb] ,360)
    pos = [fi.layout_x[i_turb], fi.layout_y[i_turb]]
    rotor_offset = rotor_diameter/100 #short distance in front of rotor
    # rotor_offset = +3
    
    #Discretize the rotor
    y_rot_discr = np.linspace(-rotor_diameter/2, rotor_diameter/2, Np_y) #extra discretization for the rotor
    z_rot_discr = np.linspace(zHub - rotor_diameter/2, zHub + rotor_diameter/2, Np_z)
    x_rot_discr = 0 + rotor_offset
    xx,yy,zz = np.meshgrid(x_rot_discr,y_rot_discr,z_rot_discr, indexing = "ij")
    actually_inside_rotor = np.sqrt( yy**2 + (zz-zHub)**2) <= rotor_diameter/2
    a = actually_inside_rotor
    

    wd_rad = np.radians(-wind_direction)      #to randians ( numpy can't handle degrees)
    transmat = np.array([[np.cos(wd_rad), -np.sin(wd_rad)],[np.sin(wd_rad),np.cos(wd_rad)]] )
    baseCoords = np.vstack((xx.flatten(), yy.flatten()))
    transCoords = transmat @ baseCoords   
    xx = transCoords[0,:].reshape(xx.shape) + pos[0]
    yy = transCoords[1,:].reshape(yy.shape) + pos[1]
    
    
    #Get the flow information
    floris_table = fi.get_set_of_points(xx.flatten(), yy.flatten(), zz.flatten())
    
    
    
    #Visualization 
    if visualization:
        ff = fi.floris.farm.flow_field
        x = ff.x.flatten()
        y = ff.y.flatten()
        z = ff.z.flatten()
        
    
        fig = plt.figure()
        ax = fig.add_subplot(projection = "3d")
        ax.scatter(x,y,z)
        ax.scatter(xx[a],yy[a],zz[a])
        ax.set_ylabel("y [m]")
        ax.set_xlabel("x [m]")
        
        # Create cubic bounding box to simulate equal aspect ratio
        if 1:
            max_range = np.array([x.max()-x.min(), y.max()-y.min(), z.max()-z.min()]).max()
            Xb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][0].flatten() + 0.5*(x.max()+x.min())
            Yb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][1].flatten() + 0.5*(y.max()+y.min())
            Zb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][2].flatten() + 0.5*(z.max()+z.min())
            # Comment or uncomment following both lines to test the fake bounding box:
            for xb, yb, zb in zip(Xb, Yb, Zb):
                ax.plot([xb], [yb], [zb], 'w')
        
    
    return floris_table
