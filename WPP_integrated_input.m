% Input for WPP integrated controller Simulink model
%
% DTU Wind Energy
% Thanasis Barlas, Qian Long, Kaushik Das, Tuhfe Gocmen
% TUM Chair of Wind energy
% Abhinav Anand, Filippo Campagnolo

%% cleaning up

close all
clear all
clc
restoredefaultpath
matlabrc

%% Wind Farm Flow model interfacing

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Run this section only when you restart MATLAB (Do not modify any line of code in this section)
% 
% % Interfacing Python interpretor to MATLAB											
% setenv('PYTHONUNBUFFERED','1');
% setenv('path',['C:\PythonVenv\Scripts;','C:\PythonVenv\', getenv('path')]);
%                        
% pe=pyenv('Version','C:\PythonVenv\Scripts\pythonw.exe',...         
%          ...'ExecutionMode','OutOfProcess'...  --- this (new mode) causes misc. issues
%          'ExecutionMode','InProcess'...  --- this (old mode) works OK
%          );
% % 
% % Adding Python module location to system path and the python module to python search path
% pyenv;
% systemPath = py.sys.path;
% pathToRunFloris = fileparts(which('run_floris.py'));
% if count(systemPath,pathToRunFloris) == 0
%     insert(systemPath,int32(0),pathToRunFloris);
% end
% py.importlib.import_module('run_floris');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% simulation

deltat = 0.01;                         % [s] sample time
t_start = 30000;                       % [s] start time
t_end = 37200;                         % [s] end time
V_init = 9;                            % [m/s] wind speed
Vdir_init = 270;                       % [deg] wind direction
VTI_init = 0.06;                       % [-] turbulence intensity
ti_flag = 1;                           % [-] flag for high-frequency turbulence fluctuations (0 = no turbulence, 1 = added turbulence)
inflow_data_flag = 1;                  % [-] flag for user-specified inflow data input (0 = constant inflow, 1 = user defined inflow)
inflow_data_file = 'InflowData.mat';   % [-] file name for inflow data (used when inflow_data_flag = 1)

%% WPP plant parameters

P_wpp_base = 60e6;                     % [W] WPP capacity
n_wt = 4;                              % [-] number of WTs
Proj_distance_init = 1200;             % [m] projected distance between two turbines

%% WPP controller

Ts = 1;                                % [s] digital control sampling time
Kp_wpp = 0.05;                         % [-] active power control proportional gain
Ki_wpp = 2.00;                         % [-] active power control integral gain
P_wpp_ref = [0.4 1.0 1.0 1.0];         % [p.u.] WPP active power reference (constant or array of switching parameters every wpp_ref_step)
deltaP_wpp_ref = [0.0 0.2 0.0 0.0];    % [p.u.] WPP delta active power reference (constant or array of switching parameters every wpp_ref_step)
wpp_ref_step = 1800;                   % [s] time step for switching P_wpp_ref and deltaP_wpp_ref (if equal to t_end-t_start, constant parameters for P_wpp_ref and deltaP_wpp_ref can be used)
yaw_ref = 0;                           % [deg] yaw angle reference (all WTs)
dP_wpp_refmax = 0.2;                   % [p.u./sec] WPP power reference maximum ramp rate
dP_wpp_refmin = -0.2;                  % [p.u./sec] WPP power reference minimum ramp rate
P_wpp_refmax = 1.0;                    % [p.u.] WPP power reference maximum limit
P_wpp_refmin = 0.3;                    % [p.u.] WPP power reference minimum limit
P_wpp_errmax = 1.0;                    % [p.u.] WPP power reference error maximum limit
P_wpp_errmin = -1.0;                   % [p.u.] WPP power reference error minimum limit
P_wpp_kimax = 1.0;                     % [p.u.] WPP power integrator error maximum limit
P_wpp_kimin = -1.0;                    % [p.u.] WPP power integrator error minimum limit
dP_wpp_kimax = 0.01;                   % [p.u./sec] WPP power integrator maximum ramp rate
dP_wpp_kimin = -0.01;                  % [p.u./sec] WPP power integrator minimum ramp rate
Kaw = 1;                               % [-] anti-windup gain

%% turbine parameters

wt_data_file = 'WT_data';              % [-] data file with Cp,Ct as a function of TSR, pitch, yaw
J= 0.313227E+09;                       % [kg*m^2] rotor-drivetrain inertia
r = 120;                               % [m] rotor radius
rho = 1.225;                           % [kg/m^2] air density
neff = 0.9655;                         % [-] drivetrain efficiency
Cp_max = 0.4892;                       % [-] max Cp (for available power)
Ct_init = 0.7978;                      % [-] initial Ct (for wind farm flow initialization)
omega_init = 0.5236;                   % [rad/s] initial rotor speed
pitch_init = 0;                        % [deg] initial pitch angle
WTdir_init = 270;                      % [deg] initial WT direction
region_init = 2;                       % [-] initial control region (2 = partial load, 3 = full load)
V_ci = 4;                              % [m/s] cut-in wind speed

%% turbine controller

pitch_file = 'wpdata.100';             % [-] min pitch data file (HAWC2 format with wind speed and pitch angle columns)
wt_data_dr_file = 'drdata.dat';        % [-] derating TSR, pitch data file for deratingMode = 3 (HAWC2 format with pitch angle, TSR, CT, CP, dCp columns)
Prated = 15e6;                         % [W] rated generator power
omega_min = 0.5236;                    % [rad/s] minimum rotor speed
omega_rated = 0.7917;                  % [rad/s] rated rotor speed
f_omega = 1*2*pi;                      % [rad/s] rotor speed filter frequency
zeta_omega = 0.7;                      % [-] rotor speed filter damping ratio
pitch_min_in = 100;                    % [deg] minimum pitch angle (>90 input file wpdata.100 is used)
pitch_max = 90;                        % [deg] maximum pitch angle
Kopt = 0.313309E+08;                   % [N*m/((rad/s)^2)] torque-speed gain
Kp_torque = 0.137765E+09;              % [N*m/(rad/s)] proportional gain of torque controller
Ki_torque = 0.309143E+08;              % [N*m/rad] integral gain of torque controller
Kp_pitch = 0.150677E+01;               % [1/s] proportional gain of pitch controller
Ki_pitch = 0.286555E+00;               % [-] integral gain of pitch controller
KK1 = 11.32216;                        % [deg] coefficient of linear term in aerodynamic gain scheduling
KK2 = 421.51156;                       % [deg^2] coefficient of quadratic term in aerodynamic gain scheduling
tau_vf = 1.0472;                       % [s] time constant of 1st order filter on wind speed used for minimum pitch
tau_pitch = 0.5236;                    % [s] time constant of 1st order filter on pitch angle for gain scheduling
rel_sp_open_Qg = 0.95;                 % [-] percentage of the rated speed when the torque limits are fully opened
TorqueCtrlRatio = 0.9;                 % [-] generator control switch (0 = constant torque, 1 = constant power, 0<x<1 blending)
pitch_lim = 2;                         % [deg] lower angle above lowest minimum pitch angle for switch
deratingMode = 1;                      % [-] derating mode (1 = constant rotor speed, 2 = max rotor speed, 3 = user defined based on dCp)
f_gen = 20*2*pi;                       % [rad/s] generator frequency
zeta_gen = 0.9;                        % [-] generator damping ratio
f_pitch = 1*2*pi;                      % [rad/s] pitch actuator frequency
zeta_pitch = 0.7;                      % [-] pitch actuator damping ratio
f_yaw = 0.2*2*pi;                      % [rad/s] yaw actuator frequency
zeta_yaw = 0.7;                        % [-] yaw actuator damping ratio
yaw_ma_start = 10.0;                   % [s] yaw start moving average window
yaw_ma_end = 1.0;                      % [s] yaw end moving average window
yaw_thres_start = 1.0;                 % [deg] yaw error threshold for starting yaw action
yaw_thres_end = 0.5;                   % [deg] yaw error threshold for stopping yaw action
yaw_rate = 0.4;                        % [deg/s] yaw rate limit
dP_wt_ref_max = 0.2;                   % [p.u./sec] WT power reference maximum ramp rate
dP_wt_ref_min = -0.2;                  % [p.u./sec] WT power reference minimum ramp rate

%% WPP collection system

% assume a radial configuration of WPP collection system with # of string (n_string) *
% # of WTs per string (n_wt_per_string)

% the code only supports the radial configuration with single or
% multiple strings with the same number of WTs in each string

% select ABB xlpe submarine cable, aluminum conductor, cross bonded with
% trefoil configuration

n_wt_per_string = 4;                   % [-] the number of WTs per string
n_string = 1;                          % [-] the number of strings
area_cross = [400, 185, 95];           % [mm^2] cross-section area, array with the size of n_string by (n_wt_per_string - 1)
l_space = [1200, 1200, 1200];          % [m] spacing length between the turbines, array with the size of n_string by (n_wt_per_string - 1)
rho_Al = 2.65E-8;                      % [omega * m] resistivity
Vbase = 66E3;                          % [V] base voltage, equal to nominal voltage of WPP collection system

%% processing

% load inflow data
load(inflow_data_file); 

% load WT data
load(wt_data_file);

% load min pitch data
if pitch_min_in>90
    formatSpec = '%f';
    wpdata = dlmread(pitch_file, formatSpec, 1, 0);
end

% load derating data
wt_data_dr= dlmread(wt_data_dr_file, formatSpec, 1, 0);

% colletion system
Sbase = P_wpp_base;                    % [W] base apparent capacity, equal to nominal capacity of one WT
Rbase = Vbase^2/Sbase;                 % [omega] base impedance
R = rho_Al .* l_space./(area_cross.*10^-6)/Rbase;   % [p.u.] cable resistance

% wind farm flow model initialization
params.num_turbines = n_wt;
ws_fs = V_init;
wd_fs = Vdir_init;
ti_fs = VTI_init;
cT = Ct_init*ones(1,n_wt);
nac_orient = Vdir_init*ones(1,n_wt);

% create wpp setpoint time series input
P_wpp_ref_ts(:,1) = t_start:wpp_ref_step:t_end-wpp_ref_step;
P_wpp_ref_ts(:,2) = P_wpp_ref';
deltaP_wpp_ref_ts(:,1) = t_start:wpp_ref_step:t_end-wpp_ref_step;
deltaP_wpp_ref_ts(:,2) = deltaP_wpp_ref';
